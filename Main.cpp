#include <iostream>
#include <unistd.h>
#include <sys/wait.h>

using namespace std;

#include "Youtube.h"

/*FUINCION MAIN*/
int main(int argc, char **argv) {

    /*VARIABLES*/
	string url; // URL del video a utilizar.
    pid_t pid; // Autorizacion de proceso hijo
    
    if (argc > 1){
        url = argv[1]; // Recibe el segundo argumento.
    } else{
       cin >> url; // El usuario ingresa la URL manualmente
    }

    Youtube y = Youtube(url); // Instanciacion de la clase Youtube
    pid = fork(); // Creacion de proceso hijo

    if (pid < 0) {
        cout << "No se pudo crear el proceso ...";
        cout << endl;
        return -1;
    
    } else if (pid == 0) { // Proceso hijo
        cout << "Proceso hijo" << endl;
        y.descarga_video();
        cout << endl;

    } else {// Proceso padre
        wait (NULL);
        cout << "Proceso padre" << endl;
        y.reproducir();
        cout << endl;
        return 0;
    }
	return 0;
}