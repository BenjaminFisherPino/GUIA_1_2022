#ifndef YOUTUBE_H
#define YOUTUBE_H

class Youtube {

    /*ATRIBUTOS*/
    private:
        string url;
        string name;
      
    public:

    /*CONSTRUCTOR*/
		Youtube(string url);

    /*METODOS*/
        void descarga_video();
        void reproducir();
};
#endif
