# GUIA 1 2022

Guía numero 1 sistema y redes 2022


## Resúmen del programa

Este programa, desarrollado por Benjamín Fisher, nos permite, dado una direccion URL de un video, descargarlo, extraerle el audio y reproducirlo con VLC. Se busca observar como se relacionan y funcionan los procesos de nuestras computadoras mediante la creacion de procesos hijo. (fork())

## Requerimientos

Para el correcto funcionamiento del programa se requiere tener instalado youtube-dl en su version 2021.12.17, vlc en su version 3.0.9.2, make en su version 4.2.1 y un compilador g++. En caso de no tener uno de estots programas el codigo no funcionara.

Si desea comprobar la version de sus programas, ingrese la siguiente linea de codigo en su terminal.

```
(programa) --version
```

En caso de tener otra version, actualice sus programas.

Si no sabe como instalar los programas, ingrese la siguiente linea de codigo en su terminal.

```
sudo apt install (programa)
```


## Instalacion

Para descargar el programa hay dos opciones. Usted puede clonar el repositorio en su computador mediante la utilizacion de git, o, puede descargar manualmente la carpeta.

Una vez descargado la carpeta, usted debe dirigirse a la ubicacion donde se aloja el programa. Aqui usted debera ingresar la siguiente linea de codigo en su terminal.


```
make
```

Este comando nos permite compilar nuestro programa y generar un ejecutable. Finalmente usted debera ingresar el siguiente comando en su terminal para ejecutar el programa.


```
./programa (URL)
```

URL es la direccion del video a utilizar. En caso de iniciar el programa sin ese parametro, se le preguntara por una URL.

## Funcionamiento del programa

EL programa nos permite dado una direccion URL, descargar un video, extraerle el audio y reproducirlo en el reproductor VLC. Sin la direccion URL, el programa no funciona.

Para ejecutar el programa se nos pide ingresar un segundo parametro, el cual es la URL. En caso de no ingresar una, el programa le preguntara al usuario por una direccion URL.

Una vez confirmado la direccion, se procedera a la creacion de un proceso hijo. Este se encarga de descargar el video y transformarlo a un archivo de audio, mientras que su proceso progenitor (proceso padre), se encarga de la reproduccion del audio. Para la creacion de este proceso hijo, se utiliza la libreria <sys/wait.h> y para la ejecucion de los comandos de descargar y reproduccion se utiliza la libreria <unistd.h>.

### Proceso hijo

El proceso hijo comienza en la linea 30 del archivo Main.cpp. En esta se nos pregunta si se autorizo la creacion del rpoceso hijo, de ser exitoso se procede a descargar y transformar el archivo a un .mp3.

### Proceso padre

El proceso padre comienza en la linea 35 del archivo Main.cpp. Este espera a la finalizacion del proceso hijo para proceder a la reproduccion de este. 

## Agradecimiento

Gracias por utilizar este programa!


## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca

Contacto
→ Mail: bfisher20@alumnos.utalca.cl

