#include <iostream>
#include <unistd.h>
#include <string>
using namespace std;

#include "Youtube.h"

/*CONSTRUCTOR*/
Youtube::Youtube(string url){
    this->url = url;
}

/*METODOS*/
void Youtube::descarga_video(){ // Fuyncion para descargar el video

    string name;
    cin >> name;
    this->name = name;
    char* n = const_cast<char*>(name.c_str());
    char* c = const_cast<char*>(this->url.c_str());
    char* param[] = {"youtube-dl", "-x", "--audio-format", "mp3", "-o", n, c, NULL};
    execvp("youtube-dl", param);
}

void Youtube::reproducir(){  // Funcion para reproducir el audio

    char* n = const_cast<char*>(this->name.c_str());
    char* param[] = {"vlc", n, NULL};
    execvp("vlc", param);

}

